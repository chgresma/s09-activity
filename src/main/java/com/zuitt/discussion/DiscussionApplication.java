package com.zuitt.discussion;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;

@SpringBootApplication
//This application will function as an endpoint that will be used in handling different Http requests

@RestController
@RequestMapping("/greeting")
//This will require all routes within the class to use the set endpoint as part of its route
public class DiscussionApplication {


	//an empty ArrayList that can store a list of String values, which can be used to store a list of enrollees for a particular course or program
	ArrayList<String> enrollees = new ArrayList<>();

	public static void main(String[] args) {
		SpringApplication.run(DiscussionApplication.class, args);
	}

	//Create a /enroll route that will accept a query string with the parameter of user which:
	@GetMapping("/enroll")
	public String enroll (@RequestParam(value="user",defaultValue = "Charmaine")String user){
		enrollees.add(user); //Adds the user’s name in the enrollees array list
		return String.format("Thank you for enrolling, %s!",user); //Returns a welcome (user)! Message
	}

	//Create a new /getEnrollees route which will return the content of the enrollees ArrayList as a string
	@GetMapping("/getEnrollees")
	public ArrayList<String> getEnrollees(){
		return enrollees;
	}

	//Create a /nameage route with that will accept multiple query string parameters of name and age and will return a Hello (name)! My age is (age). message
	@GetMapping("/nameage")
	public String nameage(@RequestParam(value="name", defaultValue = "Charmaine")String name,@RequestParam(value="age",defaultValue = "21")int age){
		return String.format("Hello %s! My age is %d.",name,age);
	}

	//Create a /courses/id dynamic route using a path variable of id that:
	@GetMapping("/course/{id}")
	public String course(@PathVariable("id")String id){

		switch(id){
			//If the path variable passed is “java101”, it will return the course name, schedule and price.
			case "java101":
				return String.format("Name: Java 101, Schedule: MWF 8:00 AM - 11:00 AM, Price: PHP 3000");

			//If the path variable passed is “sql101”, it will return the course name, schedule and price.
			case "sql101":
				return String.format("Name: SQL 101, Schedule: TTH 8:00 AM - 11:00 AM, Price: PHP 2500");

			//If the path variable passed is “javaee101”, it will return the course name, schedule and price.
			case "javaee101":
				return String.format("Name: JavaEE 101, Schedule: THF 1:30 PM - 4:00 PM, Price: PHP 3500");

			//Else return a message that the course cannot be found.
			default:
				return String.format("The course cannot be found! Please try again.");
		}
	}
}